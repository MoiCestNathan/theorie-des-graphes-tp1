from tkinter import *
from math import *
import os
from tkinter import font as tkFont
import time
import networkx as nx

maxLat = maxLong = 0
minLat = minLong = 9999

Taille_pave_X = 700
taille = round(Taille_pave_X*15.44/15.64)
Taille_pave_Y = taille
epsilon = 20

Fenetre=Tk()
Fenetre.title("Carte de France - DI4 - TP J-C. Billaut")
frame1 = Frame(Fenetre,borderwidth = 2, background = 'grey')
frame1.pack()
Label(frame1,text="TP1 Recherche des composantes connexes").pack()

Shift = 20
zone_dessin = Canvas(frame1,width=Taille_pave_X+2*Shift,height=Taille_pave_Y+2*Shift,bd=-10,background='white')


LesCouleurs = ['red', 'green', 'blue', 'yellow', 'purple', 'orange', 'cyan', 'magenta', 'brown', 'pink']

def TraceArc(ville1,ville2,col):
    x01 = (Ville_Longitude[ville1]-minLong)*ratioWidth + Shift
    y1 = (Ville_Latitude[ville1]-minLat)*ratioHeight
    x02 = (Ville_Longitude[ville2]-minLong)*ratioWidth + Shift
    y2 = (Ville_Latitude[ville2]-minLat)*ratioHeight
    y01=(Taille_pave_Y-y1)
    y02=(Taille_pave_Y-y2)
    zone_dessin.create_line(x01,y01,x02,y02,fill=col)

def PlaceVille(v,lacolor,lataille):
    x0=(Ville_Longitude[v]-minLong)*ratioWidth + Shift
    y0=((Ville_Latitude[v]-minLat)*ratioHeight)
    y0=(Taille_pave_Y)-y0
    zone_dessin.create_oval(x0-tv/2,y0-tv/2,x0+tv/2,y0+tv/2,fill=lacolor,outline=lacolor,width=lataille)

tv=2
ratioWidth = (Taille_pave_X-Shift)/(maxLong-minLong)
ratioHeight = (Taille_pave_Y-Shift)/(maxLat-minLat)
le_ratio = min(ratioWidth,ratioHeight)

cheminSommets = 'Sommets.txt'
cheminArcs = 'Arcs.txt'

LesVilles = open(cheminSommets,"r")
toutes_les_villes = LesVilles.readlines()
LesVilles.close()

LesArcs = open(cheminArcs,"r")
tous_les_arcs = LesArcs.readlines()
LesArcs.close()

Ville_Longitude = []
Ville_Latitude = []
Ville_Dpt = []

maxLat = -9999
maxLong = -9999
minLat = 9999
minLong = 9999

NbVilles = len(toutes_les_villes)
for j in range (0, NbVilles):
    cette_ville = toutes_les_villes[j].split("\t")
    ledpt = cette_ville[1]
    Ville_Dpt.append(ledpt)

    laLon = float(cette_ville[2])
    Ville_Longitude.append(laLon)
    maxLong,minLong = max(laLon,maxLong), min(laLon,minLong)

    laLat = float(cette_ville[3])
    Ville_Latitude.append(laLat)
    maxLat,minLat = max(laLat,maxLat), min(laLat,minLat)

Origine = []
Destination = []
Longueur = []

NbArcs = len(tous_les_arcs)
for u in range (0, NbArcs):
    cet_arc = tous_les_arcs[u].split("\t")
    orig = int(cet_arc[0])
    Origine.append(orig)

    dest = int(cet_arc[1])
    Destination.append(dest)

    long = float(cet_arc[2])
    Longueur.append(long)

G = nx.Graph()

for j in range(NbVilles):
    G.add_node(j)

for u in range(NbArcs):
    orig = Origine[u]
    dest = Destination[u]
    G.add_edge(orig, dest)

# After reading city data, draw each city in gray
for j in range(NbVilles):
    PlaceVille(j, 'gray', tv)

composantes_connexes = list(nx.connected_components(G))
nombre_composantes = len(composantes_connexes)

print(f"Nombre de composantes connexes : {nombre_composantes}")

for i, comp in enumerate(composantes_connexes):
    color = LesCouleurs[i % len(LesCouleurs)]
    for node in comp:
        PlaceVille(node, color, tv)
    print(f"Composante {i+1}: {len(comp)} sommets")

zone_dessin.update_idletasks()

zone_dessin.pack()
Fenetre.mainloop()
